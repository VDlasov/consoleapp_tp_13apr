﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondDay
{
    class Program
    {
        static void Method(out int x)
        {
            //Console.WriteLine("Initial X : " + x);
            x = 100;
            Console.WriteLine("Initial X : " + x);
        }

        static int Add(int num1, int num2, params int[] list)
        {
            //Console.WriteLine("Initial X : " + x);
            int sum = num1 + num2;
            foreach (var i in list)
            {
                sum += i;
            }
            return sum;
        }

        static void Main(string[] args)
        {
            //int a = 10;
            //Console.WriteLine("Acrual X : " + a);
            //Method(out a);
            //Console.WriteLine("Acrual X : " + a);



            int result = Add(10, 20, 30, 40, 50, 60);
            Console.WriteLine("Total : {0}", result);
            result = Add(10, 20, 30, 40, 50, 60, 70, 80, 90);
            Console.WriteLine("Total : {0}", result);
            

            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondDay_SecondPartOOP
{
    class Test
    {
        int scored_marks;
        static int max_marks;

        Test()
        {
            scored_marks = 10;
        }

        Test(int marks)
        {
            scored_marks = marks;
        }

        Test(Test T)
        {
            scored_marks = T.scored_marks;
        }

        static Test()
        {
            Test.max_marks = 60;
        }

        ~Test()
        {
            
        }

        void CalculatePercentage()
        {
            int perc = scored_marks*100/Test.max_marks;
            Console.WriteLine(perc);
        }

        static void Main(string[] args)
        {
            Test.max_marks = 60;

            Test t1 = new Test(38);

            Test t2 = new Test(45);

            Test t3 = new Test();

            Test t4 = new Test(t2);

            Test t5 = new Test();

            t1.CalculatePercentage();
            t2.CalculatePercentage();
            t3.CalculatePercentage();
            t4.CalculatePercentage();


            Console.ReadKey();
        }
    }
}

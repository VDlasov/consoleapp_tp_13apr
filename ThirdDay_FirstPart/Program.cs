﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThirdDay_FirstPart
{
    class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public void Work()
        {
            Console.WriteLine("It works");   
        }
    }

    class Student : Person
    {
        public new void Work()
        {
            Console.WriteLine("It studies...");
        }

        public void Method()
        {
            Console.WriteLine("It's a student method");
        }
    }

    internal class Progrem
    {
        private static void Main(string[] args)
        {
            Person p1 = new Person();
            Student s1 = new Student();
            p1.Work();
            s1.Work();
            s1.Method();

            Console.ReadKey();
        }
    }

}

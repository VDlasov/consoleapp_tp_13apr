﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThirdDay_ForthPart
{

    delegate void Del();

    class MyClass
    {
        public static void Method()
        {
            Console.WriteLine("This method is invoked through delegate");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Del d1 = new Del(MyClass.Method);
            Del d2 = MyClass.Method;

            d1();
            d2();
            Console.ReadKey();
        }
    }
}

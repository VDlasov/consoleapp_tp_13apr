﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThirdDay_SecondPart
{
interface IAthelete
    {
        void Run();
        void WorkOut();
    }
    abstract class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public abstract void Work();
    }
    class Student : Person, IAthelete
    {

        public override void Work()
        {
            Console.WriteLine("It studies...");
        }

        public void Run()
        {
            Console.WriteLine("");
        }

        public void WorkOut()
        {
            Console.WriteLine("");
        }
    }
    class Employee : Person
    {
        public override void Work()
        {
            Console.WriteLine("It earns...");
        }
    }
    class Manager : Employee
    {
        public sealed override void Work()
        {
            Console.WriteLine("It manages a team...");
        }
    }
    class GeneralManager : Manager
    {

    }

    internal class Program
    {
        private static void Main(string[] args)
        {
            Person s1 = new Student();
            s1.Work();
            s1 = new Employee();
            s1.Work();
            s1 = new Manager();
            s1.Work();
            Student s = new Student();
            Console.ReadKey();
        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThirdDay_ThirdPart
{
    class Account
    {
        float ini_amt;
        static float int_rate;

        static Account()
        {
            Account.int_rate = 5.55f;
        }

        public static float InterestRate
        {
            get { return int_rate; }
        }

        public float InitialAmount
        {
            set
            {
                if (value < 10000)
                {
                    Console.WriteLine("InitialAmount amount cannot be less than 10000");
                    return;
                }
                ini_amt = value;
            }
            get { return ini_amt; }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Account.InterestRate = 8.5f;
            Console.WriteLine(Account.InterestRate);


            Account ac = new Account();
            //ac.int_amt = -10000;
            ac.InitialAmount = 1000;
            Console.WriteLine(ac.InitialAmount);
            Console.ReadKey();
        }
    }
}

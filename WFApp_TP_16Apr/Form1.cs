﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Lib_TP_16Apr;

namespace WFApp_TP_16Apr
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            UserLogin login = new UserLogin();
            if (login.AuthenicateUser(textBox1.Text, textBox2.Text))
                MessageBox.Show(login.WelcomeUser(textBox1.Text));
            else
                MessageBox.Show("Incorrect Username / Password");
        }
    }
}
